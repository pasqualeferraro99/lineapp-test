var counter = 0;
function changeMode(){
    var element = document.body;
    var stampa = document.getElementsByTagName('p')
    element.classList.toggle('dark-mode')
    counter++;
    document.getElementById("paragrafo").innerHTML = "Il bottone è stato cliccato: " + counter + " volte"
    document.getElementsByTagName("p")[1].innerHTML = "Stato attuale : " + (counter%2===0 ? "Light" : "Dark")
    aggiornaBottone();
}
function aggiornaBottone(){
    document.getElementsByClassName("btn")[0].innerHTML = "Go " + (counter%2===0 ? "Dark" : "Light")
    document.getElementsByClassName("btn")[0].classList.toggle("btn-light")
}